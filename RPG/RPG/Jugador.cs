﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    abstract class Jugador
    {
        protected string name;
        protected float fuerza;
        protected float vida;
        protected float destreza;
        protected Jugador(string name, float fuerza, float vida, float destreza) 
        {
            this.name = name;
            this.fuerza = fuerza;
            this.vida = vida;
            this.destreza = destreza;
        }
        
        public string Estado()
        {
            return $"Nombre:{name},Vida:{vida},Fuerza:{fuerza},Destreza:{destreza}";
        }
        
    }


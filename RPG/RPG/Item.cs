﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class Item: Jugador, IEItemdeDestreza,IEItemdeFuerza,IEItemdeVida   
    {
        private float cantidadV;
        private float cantidadF;
        private float cantidadD;
        public Item(float fuerza,float destreza,float vida,float cantidadV,float cantidadF,float cantidadD) : base("",fuerza,vida,destreza) 
        {
            this.cantidadD = cantidadD;
            this.cantidadV = cantidadD;
            this.cantidadF = cantidadD;
        }
        public float RecuperarVida()
        {
            return vida + 50*cantidadF;
        }
        public float RecuperarDestreza()
        {
        return destreza + 50 * cantidadD;
        }
        public float RecuperarFuerza()
        {
        return fuerza + 50 * cantidadD;
        }
        public float VidaoMuerte()
        {
            return vida;
        }    
}
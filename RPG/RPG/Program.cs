﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG
{
    class Program
    {
        static void Main(string[] args)
        {
            string decision;
            string capacidad;
            string fuerza;
            string destreza;
            string vida;
            float fuerzaT;
            float destrezaT;
            float vidaT;
            float cantidadV = 0;
            float cantidadF = 0;
            float cantidadD = 0;
            Console.WriteLine("Bienvenido al laberinto. Primero, crea tus estadísticas. No pueden sumar más de 100 en total");
            Console.WriteLine("Fuerza:");
            fuerza = Console.ReadLine();
            fuerzaT = Int32.Parse(fuerza);
            Console.WriteLine("Destreza:");
            destreza = Console.ReadLine();
            destrezaT = Int32.Parse(destreza);
            Console.WriteLine("Vida:");
            vida = Console.ReadLine();
            vidaT = Int32.Parse(vida);

            IEItemdeVida jugador = new Item(fuerzaT, destrezaT, vidaT, cantidadV, cantidadF, cantidadD);

            Console.WriteLine("Primero, te encuentras en la entrada de este. Adelante de ti, hay 2 puertas. Elige la puerta(Izquierda o Derecha).Una vez dentro, no puedes Retroceder");
            decision = Console.ReadLine();

            //1
            if (decision == "Izquierda")
            {
                Console.WriteLine($"Haz elegido {decision}. Encuentras un objeto de +50 vida. Decides llevartelo y seguir avanzando");
                cantidadV += 1;
            }
            else if (decision == "Derecha")
            {
                Console.WriteLine($"Haz elegido {decision}. Encuentras un acertijo que requiere 20 de Destreza o -40 de vida");
                if (destrezaT <= 20)
                {
                    vidaT -= 40;
                }
                if (vidaT <=0) 
                {
                    Console.WriteLine("Es el fin. No puedes progresar ni salir del Laberinto, no tienes la energía para eso. Se cierrran las puertas de atrás y te quedas atrapado...hasta tu muerte...");
                    Console.WriteLine("Final: Muerte");
                    decision = "Fin del Juego";
                }


            }
            else if(decision == "Retroceder") 
            {
                Console.WriteLine("Decides no arriesgar tu vida y das un paso hacia atrás. Hasta hoy, el tesoro del laberinto para nunca haber sido descubierto. Tal vez era lo mejor para ti...");
                Console.WriteLine("Final: Nada");
                decision = "Fin del Juego";
            }
            if(vidaT>0)
            {
            Console.WriteLine("Adelante, llegas a otras 2 puertas. Respiras y con calma decides...");
            decision = Console.ReadLine();
            }
            //2
            if (decision == "Izquierda")
            {
                Console.WriteLine($"Haz elegido {decision}. De repente, el suelo se abre y comienza a brotar lava. Te las arreglas para estar en una zona segura, pero para subir, necesitas escalar con 30 de fuerza o -60 de daño");
                if (fuerzaT  <= 30)
                {
                    vidaT -= 60;
                }
                
                if (cantidadV > 0)
                {
                    jugador.RecuperarVida();
                    cantidadV -= 1;
                }

                if (vidaT <= 0)
                {
                    Console.WriteLine("Es el fin. No puedes progresar ni salir del Laberinto, no tienes la energía para eso. Se cierrran las puertas de atrás y te quedas atrapado...hasta tu muerte...");
                    Console.WriteLine("Final: Muerte");
                    decision = "Fin del Juego";
                }
            }


            else if (decision == "Derecha")
            {
                Console.WriteLine($"Haz elegido {decision} .Parece una sala tranquila, puedes seguir adelante");
            }

            //3
            if (vidaT > 0)
            {
                Console.WriteLine("El tesoro, lograste encontrarlo, son grandes cofres de oro y llenos de más oro. Oyes un ruido, pero de adelante, parece que una puerta de salida esta ante ti. Lo lograste");
                Console.WriteLine("...una nota esta junto al cofre más grande del cuarto y dice: Me venciste, esto es todo tuyo.");
                Console.WriteLine("Final: Ganador");
                decision = "Fin del Juego";
            }

            Console.ReadLine();
        }
    }
}
